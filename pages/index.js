"use strict";

import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import './style.css'
var AuditLatest = null;
try {
  AuditLatest = require('./audit-latest.json');
} catch (e) {}

function getCratePath(name) {
  name = name.toLowerCase();
  switch (name.length) {
    case 1:
      return "1/" + name;
    case 2:
      return "2/" + name;
    case 3:
      return "3/" + name.charAt(0) + "/" + name;
    default:
      return name.slice(0, 2) + "/" + name.slice(2, 4) + "/" + name;
  }
}

class CrateAudit extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isExpanded: false, crateInfo: null };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isExpanded: !this.state.isExpanded
    }));
  }

  render() {
    const crateName = this.props.name;
    const crateAdvisories = this.props.advisories;
    const crateInfo = this.state.crateInfo;
    if (this.state.isExpanded && this.state.crateInfo === null) {
      const cratePath = getCratePath(crateName);
      fetch(
        `https://raw.githubusercontent.com/rust-lang/crates.io-index/master/${cratePath}`
      )
        .then(response => response.text())
        .then(responseText => {
          let responseLines = responseText.split("\n");
          let responseJson = responseLines.pop();
          while (responseJson === "") {
            responseJson = responseLines.pop();
          }
          try {
            const crateInfo = JSON.parse(responseJson);
            this.setState(state => ({ crateInfo }));
          } catch (e) {
            console.log("response: ", responseText);
            console.log(e);
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
    return (
      <div className="advisory" key={crateName}>
        <div className="overview" onClick={this.handleClick}>
          {crateName} ({crateAdvisories.length}){" "}
          {this.state.isExpanded &&
            this.state.crateInfo !== null &&
            "v" + crateInfo.vers}
          <div className="arrow">{this.state.isExpanded ? "▲" : "▼"}</div>
        </div>
        {this.state.isExpanded && (
          <ul>
            {crateAdvisories.map(advisory => (
              <li key={advisory}>
                <a href={"https://rustsec.org/advisories/" + advisory}>
                  {advisory}
                </a>
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { audit: AuditLatest, filter: "" };
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  componentDidMount() {
    if (this.state.audit !== null)
        return;
    fetch("https://crates.rustsec.org/audit-latest.json", {
      mode: "cors"
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          audit: responseJson
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleFilterChange(event) {
    this.setState({ filter: event.target.value });
  }

  render() {
    let advisories = [];
    if (this.state.audit) {
      const filter = this.state.filter.toLowerCase();
      for (let [crateName, crate_advisories] of Object.entries(
        this.state.audit.advisories
      )) {
        if (
          filter.length != 0 &&
          crateName.toLowerCase().indexOf(filter) === -1
        ) {
          continue;
        }
        advisories.push(
          <CrateAudit
            key={crateName}
            name={crateName}
            advisories={crate_advisories}
          />
        );
      }
    }
    return (
      <div className="container">
        <div className="title">
          <h1>Crates Audit</h1>
          <h3>
            {advisories.length > 0 && advisories.length} Crates with advisories
          </h3>
          <input
            type="text"
            placeholder="Filter"
            value={this.state.filter}
            onChange={this.handleFilterChange}
          />
        </div>
        <div className="advisories">{advisories}</div>
      </div>
    );
  }
}

const domContainer = document.querySelector("#root");
ReactDOM.render(<App />, domContainer);
