extern crate bincode;
extern crate directories;
extern crate getopts;
extern crate pbr;
extern crate rayon;
extern crate rustsec;
extern crate semver;
extern crate serde;
extern crate serde_json;
extern crate toml;
#[macro_use]
extern crate serde_derive;

mod canonical;
mod index;
mod resolver;

use std::collections::BTreeMap;
use std::env;
use std::fs::{read_dir, write};
use std::path::{Path, PathBuf};
use std::process::Command;
use std::str::FromStr;
use std::sync::{
    atomic::{AtomicUsize, Ordering, ATOMIC_USIZE_INIT},
    Arc,
};
use std::thread;
use std::time::Duration;

use directories::UserDirs;
use getopts::Options;
use pbr::ProgressBar;
use rayon::prelude::*;
use rustsec::{advisory::Advisory, db::AdvisoryDatabase, package::PackageName};

use canonical::*;
use index::*;
use resolver::*;

fn update_upstream(crates_index_path: &Path) -> bool {
    let crates_index_str = crates_index_path.to_str().unwrap();
    if crates_index_path.exists() {
        println!("Syncing crates.io index");
        let fetch_status = Command::new("git")
            .args(&["-C", crates_index_str, "fetch", "origin", "master"])
            .status();
        match fetch_status {
            Ok(status) => {
                if !status.success() {
                    println!("Non-zero exit code returned from `git fetch`");
                    return false;
                }
            }
            Err(e) => {
                println!("Failed to execute `git fetch`: {}", e);
                return false;
            }
        }
        let checkout_status = Command::new("git")
            .args(&["-C", crates_index_str, "checkout", "-f", "FETCH_HEAD"])
            .status();
        match checkout_status {
            Ok(status) => {
                if status.success() {
                    true
                } else {
                    println!("Non-zero exit code returned from `git checkout`");
                    false
                }
            }
            Err(e) => {
                println!("Failed to execute `git checkout`: {}", e);
                false
            }
        }
    } else {
        println!("Cloning crates.io index");
        let clone_status = Command::new("git")
            .args(&["clone", UPSTREAM_CRATES_URL, crates_index_str])
            .status();
        match clone_status {
            Ok(status) => {
                if status.success() {
                    true
                } else {
                    println!("Non-zero exit code returned from `git clone`");
                    false
                }
            }
            Err(e) => {
                println!("Failed to execute `git clone`: {}", e);
                false
            }
        }
    }
}

pub struct Progress {
    max: usize,
    count: Arc<AtomicUsize>,
    joiner: Option<thread::JoinHandle<()>>,
}

impl Clone for Progress {
    fn clone(&self) -> Self {
        Progress {
            max: self.max,
            count: self.count.clone(),
            joiner: None,
        }
    }
}

impl Progress {
    pub fn new(enabled: bool, max: usize, finish_message: &'static str) -> Progress {
        let count = Arc::new(ATOMIC_USIZE_INIT);
        let joiner = if enabled {
            let count = count.clone();
            Some(thread::spawn(move || {
                let mut pb = ProgressBar::new(max as u64);
                loop {
                    let current_count = count.load(Ordering::Relaxed);
                    if current_count >= max {
                        pb.finish_print(finish_message);
                        return;
                    }
                    pb.set(current_count as u64);
                    thread::sleep(Duration::from_millis(1));
                }
            }))
        } else {
            None
        };

        Progress { max, count, joiner }
    }

    pub fn inc(&self) {
        self.count.fetch_add(1, Ordering::Relaxed);
    }
}

impl Drop for Progress {
    fn drop(&mut self) {
        if let Some(joiner) = self.joiner.take() {
            self.count.store(self.max, Ordering::Relaxed);
            let _ = joiner.join();
        }
    }
}

fn load_index_cache(enable_progress: bool, crates_index_path: &Path) -> IndexCache {
    let mut visit_dirs = vec![PathBuf::from(crates_index_path)];
    // About double the crates as of Oct 2018.
    let mut crates_index_paths = Vec::with_capacity(40000);

    while let Some(dir) = visit_dirs.pop() {
        for entry in read_dir(dir).unwrap() {
            let entry = entry.unwrap();
            let path = entry.path();
            if path.is_dir() {
                if entry.file_name() == ".git" {
                    continue;
                }
                visit_dirs.push(path);
            } else {
                let name = entry.file_name().into_string().unwrap();
                if name == "config.json" {
                    continue;
                }
                crates_index_paths.push((name, path));
            }
        }
    }

    let progress = Progress::new(
        enable_progress,
        crates_index_paths.len(),
        "Finished loading crate index.",
    );

    let index_map = crates_index_paths
        .into_par_iter()
        .map(|(name, path)| {
            progress.inc();
            (name, IndexCrate::from_file(&path))
        }).collect();

    IndexCache::new(index_map)
}

enum CrateAuditResult<'a> {
    Ok,
    ResolveError(ResolveError),
    Advisories(Vec<&'a Advisory>),
}

impl<'a> CrateAuditResult<'a> {
    fn has_advisories(&self) -> bool {
        match self {
            CrateAuditResult::Advisories(_) => true,
            _ => false,
        }
    }
}

const CRATES_AUDIT_FORMAT_VERSION: usize = 1;
#[derive(Serialize, Deserialize, PartialEq)]
struct CratesAudit {
    version: usize,
    crates_index_commit: String,
    advisory_db_commit: String,
    advisories: BTreeMap<String, Vec<String>>,
}

fn print_usage(opts: Options) {
    let brief = format!("Usage: crates-audit [options]");
    print!("{}", opts.usage(&brief));
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut opts = Options::new();
    opts.optflag(
        "u",
        "update",
        "Update the local index before other operations.",
    );
    opts.optmulti(
        "o",
        "output",
        "Output audit results to the given file ending in `.bin`, `.toml`, or `.json`.",
        "FILE",
    );
    opts.optflagmulti("v", "verbose", "Use verbose output");
    opts.optflag("q", "quiet", "No output printed to stdout");
    opts.optflag("", "no-progress", "No progress bars.");
    opts.optflag("h", "help", "Display this message");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };

    if matches.opt_present("h") {
        print_usage(opts);
        return;
    }

    let quiet = matches.opt_present("q");
    let enable_progress = !quiet && !matches.opt_present("no-progress");

    let crates_index_path = if let Some(cargo_home) = env::var_os("CARGO_HOME") {
        PathBuf::from(cargo_home).join(CRATES_AUDIT_INDEX)
    } else if let Some(dirs) = UserDirs::new() {
        dirs.home_dir().join(".cargo").join(CRATES_AUDIT_INDEX)
    } else {
        panic!("Failed to find crates-audit-index.");
    };

    if matches.opt_present("u") || !crates_index_path.exists() {
        if !update_upstream(&crates_index_path) {
            return;
        }
    }

    let crates_index = load_index_cache(enable_progress, &crates_index_path);
    let advisory_db = AdvisoryDatabase::fetch().unwrap();

    if !quiet {
        println!("Resolving {} crates.", crates_index.count());
    }

    let all_crates: Vec<_> = crates_index.iter_indexed_crates().collect();

    let progress = Progress::new(
        enable_progress,
        crates_index.count(),
        "Finished resolving all crates.",
    );
    let all_crate_results: BTreeMap<_, _> = all_crates
        .into_par_iter()
        .map(|index_crate| {
            let entry = index_crate.newest_entry().unwrap();
            let resolved_deps = match resolve_deps(&crates_index, &entry) {
                Ok(d) => d,
                Err(e) => return (entry.name.clone(), CrateAuditResult::ResolveError(e)),
            };
            progress.inc();
            let mut advisories = Vec::new();
            for dep in &resolved_deps {
                let dep_advisories =
                    advisory_db.advisories_for_crate(PackageName(dep.name.clone()), &dep.vers);
                advisories.extend(dep_advisories.into_iter());
            }
            if advisories.is_empty() {
                (entry.name.clone(), CrateAuditResult::Ok)
            } else {
                advisories.sort_by_key(|a| a.id.as_str());
                advisories.dedup();
                (entry.name.clone(), CrateAuditResult::Advisories(advisories))
            }
        }).collect();
    drop(progress);

    if !quiet {
        println!(
            "Found {} crates with advisories.",
            all_crate_results
                .values()
                .filter(|r| r.has_advisories())
                .count()
        );
    }

    if matches.opt_count("o") > 0 {
        let advisories = all_crate_results
            .into_par_iter()
            .filter_map(|(k, v)| {
                if let CrateAuditResult::Advisories(advisories) = v {
                    Some((k, advisories.iter().map(|a| a.id.to_string()).collect()))
                } else {
                    None
                }
            }).collect();
        let audit = CratesAudit {
            version: CRATES_AUDIT_FORMAT_VERSION,
            crates_index_commit: "dummy".to_owned(),
            advisory_db_commit: "dummy".to_owned(),
            advisories,
        };
        for output in matches.opt_strs("o") {
            if !quiet {
                println!("Writing output `{}`.", output);
            }
            if output.ends_with(".bin") {
                write(output, bincode::serialize(&audit).unwrap()).unwrap();
            } else if output.ends_with(".json") {
                write(output, serde_json::to_vec(&audit).unwrap()).unwrap();
            } else if output.ends_with(".toml") {
                write(output, toml::to_vec(&audit).unwrap()).unwrap();
            } else {
                panic!("Unrecognized format for `{}`.", output)
            }
        }
    }
}
